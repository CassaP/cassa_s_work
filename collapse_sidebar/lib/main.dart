import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Collapse Bar Menu',
      home: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          backgroundColor: Color.fromARGB(250, 28, 63, 95),
          title: Text(
            'Client Side Dev',
          ),
        ),
        body: Container(
          color: Color(0xFFB1F2B36),
          child: AppContainer(),
        ),
      ),
    );
  }
}

class AppContainer extends StatefulWidget {
  const AppContainer({Key? key}) : super(key: key);

  @override
  State<AppContainer> createState() => _AppContainerState();
}

class _AppContainerState extends State<AppContainer> {
  final List<String> menuItems = [
    "Home",
    "Business Profile",
    "Calendar",
    "Facility",
    "Registration",
    "Personal Profile",
    "Meetings",
    "Theme Settings",
  ];

  final List<String> menuIcons = [
    "home",
    "save",
    "download",
    "home",
    "date",
    "download",
    "save",
    "date",
  ];

  bool sidebarOpen = false;

  double yOffset = 0;
  double xOffset = 60;

  int selectedMenuItem = 0;
  String page = "Home";

  void setSidebarState() {
    setState(() {
      xOffset = sidebarOpen ? 265 : 60;
    });
  }

  void setPage() {
    switch (selectedMenuItem) {
      case 0:
        page = "Home";
        break;
      case 1:
        page = "Business Profile";
        break;
      case 2:
        page = "Calendar";
        break;
      case 3:
        page = "Facility";
        break;
      case 4:
        page = "Registration";
        break;
      case 5:
        page = "Personal Profile";
        break;
      case 6:
        page = "Meetings";
        break;
      case 7:
        page = "Theme Settings";
        break;
      default:
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(
        children: [
          Container(
            width: double.infinity,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              mainAxisSize: MainAxisSize.max,
              children: [
                Container(
                  child: Container(
                    color: Color(0xFFB1F2B36),
                    child: Row(
                      children: [
                        Container(
                          padding: const EdgeInsets.all(22),
                          child: null,
                        ),
                        Container(
                          child: Expanded(
                            child: Center(
                              child: UserAccountsDrawerHeader(
                                accountName: Text('Rick Kamble'),
                                accountEmail: Text('rick.k@idigicloudtech.com'),
                                currentAccountPicture: CircleAvatar(
                                  child: ClipOval(
                                    child: Image.asset('images/rick.jpg'),
                                  ),
                                ),
                                decoration: BoxDecoration(
                                  color: Color(0xFFB1F2B36),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  child: Row(
                    children: [
                      Container(
                        padding: const EdgeInsets.all(20),
                        child: Icon(
                          Icons.search,
                          color: Colors.white,
                        ),
                      ),
                      Container(
                        child: Expanded(
                          child: TextField(
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              contentPadding: const EdgeInsets.all(20),
                              hintText: "Filter menu here...",
                              hintStyle: TextStyle(
                                color: Color(
                                  0xFFB666666,
                                ),
                              ),
                            ),
                            style: TextStyle(
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  child: Expanded(
                    child: new ListView.builder(
                      itemCount: menuItems.length,
                      itemBuilder: (context, index) => GestureDetector(
                        onTap: () {
                          selectedMenuItem = index;
                          setSidebarState();
                          setPage();
                        },
                        child: MenuItem(
                          itemIcon: menuIcons[index],
                          itemText: menuItems[index],
                          selected: selectedMenuItem,
                          position: index,
                        ),
                      ),
                    ),
                  ),
                ),
                Container(
                  child: Column(
                    children: [
                      Container(
                        height: 65,
                        child: Row(
                          children: [
                            GestureDetector(
                              onTap: (() {
                                sidebarOpen = !sidebarOpen;
                                setSidebarState();
                              }),
                              child: Container(
                                padding: EdgeInsets.all(15),
                                child: Icon(
                                  Icons.double_arrow_rounded,
                                  color: Colors.white,
                                  size: 25,
                                ),
                              ),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
          AnimatedContainer(
            curve: Curves.easeInOut,
            duration: Duration(milliseconds: 300),
            transform: Matrix4.translationValues(xOffset, yOffset, 1.0),
            width: double.infinity,
            height: double.infinity,
            color: Colors.white,
            child: Center(
              child: Text(page),
            ),
          )
        ],
      ),
    );
  }
}

class MenuItem extends StatelessWidget {
  final String? itemText;
  final String? itemIcon;
  final int? selected;
  final int? position;

  MenuItem({this.itemText, this.itemIcon, this.selected, this.position});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: selected == position ? Color(0xFFB151E26) : Color(0xFFB1F2B36),
      child: Row(
        children: [
          Container(
            width: 62,
            height: 62,
            padding: EdgeInsets.all(20),
            child: Image.asset(
              "images/${itemIcon}.png",
              color: Colors.white,
            ),
          ),
          Container(
            padding: EdgeInsets.all(20),
            child: Row(
              children: [
                Text(
                  itemText!,
                  style: TextStyle(
                    fontSize: 16,
                    color: Colors.white,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
