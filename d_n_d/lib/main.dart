// POC - Drag and Drop Functionality
// using Draggable and DragTarget Widget

import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    bool flag = false;
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Drag & Drop'),
        ),
        body: Center(
          child: Column(
            children: [
              // Draggable allows us to drag a widget across screen.

              Draggable(
                // Whatever is inside the 'child' will be the property that is displayed when the widget(Draggable) is stationary.
                child: Container(
                  width: 100,
                  height: 100,
                  color: Colors.amber,
                ),

                //  Whatever is inside the 'feedback' will be the property that will be displayed when widget(Draggable) is being dragged.
                feedback: Container(
                  child: Text(
                    'I am being dragged.',
                    style: TextStyle(
                      fontSize: 24,
                    ),
                  ),
                  width: 100,
                  height: 100,
                  color: Colors.teal[200],
                ),

                // Whatever is inside the 'childWhenDragging' will be the property that will be displayed -:
                // as a left behind child when widget(Draggable) is being dragged.
                childWhenDragging: Container(
                  width: 100,
                  height: 100,
                  color: Colors.grey[600],
                ),

                // axis: Axis.vertical, (Optional)'axis' property allows us to restrict the movement of :-
                //widget(Draggable) in vertically or horizontally.

                // 'maxSimultaneousDrags' property specifies how many times the widget(Draggable) can be dragged
                maxSimultaneousDrags: 1,
                data: 'iDigiCloud Technologies Pvt Ltd.',
              ),

              SizedBox(
                height: 200,
              ),

              // DragTarget provides destination for Draggable widget
              DragTarget(
                // 'builder' property builds the widget inside the drag target and is called everytime the Draggable :-
                // widget is hovered or dropped into the DragTarget
                builder: (context, acceptedData, rejectedData) {
                  return flag
                      ? Container(
                          child: Text(
                            'I am Accepted by DataTarget',
                            style: TextStyle(
                              fontSize: 20,
                            ),
                          ),
                          width: 100,
                          height: 100,
                          color: Colors.lightGreen,
                        )
                      : Container(
                          child: Text(
                            'Data Target',
                            style: TextStyle(
                              fontSize: 24,
                            ),
                          ),
                          width: 100,
                          height: 100,
                          color: Colors.pink,
                        );
                },

                // 'onWillAccept property takes the actual data that is being passed by the Draggable :-
                // and determines whether to accept or reject it.'
                onWillAccept: (data) {
                  print('The data is being dragged from above to box below.');
                  return true;
                },

                // 'onAccept' property also takes the actual data that is being passed by the Draggable :-
                // and depending upon the logic written, it process the data accordingly
                onAccept: (data) {
                  flag = true;

                  print(data);
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
