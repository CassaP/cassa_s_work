import 'dart:math' as math;

void main(List<String> arguments) {
  int min = -1, max = 2;

  // The code line below will always generate a number between -1 to 1
  // nextInt method generates a non-negative number between 0(inclusive) to max(exclusive)
  int zero = min + math.Random().nextInt(max - min);

  // We are excepting zero to have 0 as value, if not exceptions are thrown
  print('Random zero: $zero');

  try {
    // if number generated is less than zero Negative Value error is thrown and vice versa
    if (zero < 0) {
      throw NegativeValue(message: 'Negative Value');
    } else if (zero > 0) {
      throw PositiveValue(message: 'Positive value');
    }
  } on NegativeValue {
    print('The value is negative');
  } catch (e) {
    if (e is PositiveValue) {
      print('The value is positive');
      rethrow;
    }
  } finally {
    zero = 0;
  }
  if (zero == 0) {
    print('Zero at the end: $zero');
  }
}

// Negative Value exception
class NegativeValue implements Exception {
  final String message;
  NegativeValue({required this.message});
}

// Positive Value exception
class PositiveValue implements Exception {
  final String message;
  PositiveValue({required this.message});
}
