import 'package:flutter/material.dart';

class Answer extends StatelessWidget {
  final VoidCallback selectEvent;
  final String ansText;

  Answer(this.selectEvent, this.ansText);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.all(10),
      child: ElevatedButton(
          style: ButtonStyle(
              backgroundColor: MaterialStateProperty.all(Colors.blue)),
          child: Text(ansText),
          onPressed: selectEvent),
    );
  }
}
