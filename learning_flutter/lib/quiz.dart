import 'package:flutter/material.dart';
import './answer.dart';
import './question.dart';

class Quiz extends StatelessWidget {
  final List<Map<String, Object>> questions;
  final VoidCallback answerQuestion;
  final int questionIndex;

  Quiz({this.questions, this.answerQuestion, this.questionIndex});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Question(questions[questionIndex]['queText'] as String),
        ...(questions[questionIndex]['ans'] as List<String>).map((ans) {
          return Answer(answerQuestion, ans);
        }).toList()
      ],
    );
  }
}
