import 'package:flutter/material.dart';
import './quiz.dart';
import './result.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _MyAppState();
  }
}

class _MyAppState extends State<MyApp> {
  var _queIndex = 0;

  static const _queBank = [
    {
      'queText': 'What is your favourite color ?',
      'ans': ['Black', 'Blue', 'White', 'Teal'],
    },
    {
      'queText': 'What is your favourite animal ?',
      'ans': ['Dog', 'Cat', 'Tiger', 'Lion'],
    },
    {
      'queText': 'What is your favourite city ?',
      'ans': ['Nagpur', 'Mumbai', 'Pune', 'Delhi'],
    },
  ];

  void _resetHandler() {
    setState(() {
      _queIndex = 0;
    });
  }

  void _answerKey() {
    setState(() {
      _queIndex += 1;
    });
    // print(_queIndex);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Quiz App'),
        ),
        body: _queIndex < _queBank.length
            ? Quiz(
                answerQuestion: _answerKey,
                questions: _queBank,
                questionIndex: _queIndex,
              )
            : Result(_resetHandler),
      ),
    );
  }
}
