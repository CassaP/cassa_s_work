import 'package:flutter/material.dart';

class Result extends StatelessWidget {
  final VoidCallback resetHandler;

  Result(this.resetHandler);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text('You have attempted all the questions !'),
          OutlinedButton(
            child: Text('Reset Quiz'),
            onPressed: resetHandler,
          )
        ],
      ),
    );
  }
}
