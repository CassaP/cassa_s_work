import 'package:flutter/material.dart';
import 'package:submenu_context/model/menu_item.dart';

class MenuItems {
  static const List<MenuItem> itemFirst = [
    itemSettings,
    itemHome,
    itemProfile,
  ];
  static const itemSettings = MenuItem(text: "Settings", icon: Icons.settings);
  static const itemHome = MenuItem(text: "Home", icon: Icons.home);
  static const itemProfile = MenuItem(text: "Profile", icon: Icons.person);
}
