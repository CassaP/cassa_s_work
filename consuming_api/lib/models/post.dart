import 'dart:convert';

List<Post> postFromJson(String str) =>
    List<Post>.from(json.decode(str).map((x) => Post.fromJson(x)));

String postToJson(List<Post> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Post {
  Post({
    required this.username,
    required this.email,
    required this.name,
  });

  String username;
  String email;
  String name;

  factory Post.fromJson(Map<String, dynamic> json) => Post(
        username: json['username'],
        email: json['email'],
        name: json['name'],
      );

  Map<String, dynamic> toJson() => {
        'username': username,
        'email': email,
        'name': name,
      };
}
