
# Cassa's World - Dive into the backdrop of Front End Development with me !
### - This repository is the collection of all the tasks and POCs that I did in the company !
#### - Note : This has been written in a funny way so if anything written further is found to be offensive, please accredit it to my naivety and juvenile nature. Also this repository is for tracking my personal work and hence not to be confused with branching with main idc-cbs branch.

```
cd existing_repo
git remote add origin https://gitlab.com/CassaP/cassa_s_work.git
git branch -M main
git push -uf origin main
```

## Blessed this company with my presence from 7th February 2022 AD.
#### If you are reading this then you should know 'Ser Cassablonka P' was here.


## S-P-R-I-N-T-S : 
#### Lets delve more into the awesome work that I carry out at IDC.
#### You will find my work/tasks/pocs that I did in my each sprint below.
		Are you ready ? ( It's a rhetorical question,so even if you are not, idc. )

## Sprint Cycle - 9th February to 23th February 2022
#### Tasks completed in that Sprint :

    1. POC - Adobe XD to Flutter
		a. XD Design
		b. Flutter Code
		c. Flutter Output
	
	2. POC - Investigate FlutterFlow
		a. Basic Design - https://app.flutterflow.io/share/login-page-5bx1g2
		b. Premium FlutterFlow 
			i. Simple UI Screens - https://app.flutterflow.io/run/7D0fAWXVOOMSeyyOCvy7
			ii. Login Authentication - https://app.flutterflow.io/run/XjfPTHwLktHOQHVfVSWy

    3. FlutterFlow Documentation - Final Verdict ( Document attached in the main branch.  Doc Name : FlutterFlow )

## Sprint Cycle - 24th February to 9th March 2022
#### Tasks completed in that Sprint :

	1. Started with Learning Flutter ( PoW - dir_name: learning_flutter )
	2. POC - Drag and Drop ( PoW - dir_name: d_n_d )
	3. Moved to Dart Learning
	4. Dart Learning Roadmap ( Document attached in the main branch. Doc Name : Dart Learning Roadmap )
	5. Exception handling in Dart - ( PoW - dir_name: exceptions )
	6. API Learning

## Sprint Cycle - 10th March to 23th March 2022
#### Tasks completed in that Sprint :

	1. POC - Collapsable Sidebar ( PoW - dir_name: collapse_sidebar)
	2. POC - Context Specific Menu ( PoW - dir_name: context_specific_menu )
	3. Started learning about existing Client Dev Application 'idc/userapp'

## Sprint Cycle - 24th March to 6th April 2022
#### Tasks completed in that Sprint :

	1. POC - Notification Bars on success/warnings/error,etc
		- Implemented Alert Dialog Box, Snack Bar and Toast display for notification.
			( PoW - dir_name: notification_bars ) 
	2. Consumming API in flutter app
		- Used Rest Api to show output on the flutter screen
			( PoW - dir_name: consuming_api)
