import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

void main() => runApp(const MyApp());

/// This is the main application widget.
class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  static const String _title = 'Notification Bars';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: _title,
      home: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: const Text(_title),
          backgroundColor: Colors.blueGrey[900],
        ),
        body: const Center(
          child: MyStatelessWidget(),
        ),
      ),
    );
  }
}

/// This is the stateless widget that the main application instantiates.
class MyStatelessWidget extends StatelessWidget {
  const MyStatelessWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        TextButton(
          onPressed: () => showDialog<String>(
            context: context,
            builder: (BuildContext context) => AlertDialog(
              title: const Text('Welcome to iDC World !'),
              content: const Text(
                  'The page you are trying to view is corrupted, Do you still wish to proceed ?'),
              actions: <Widget>[
                TextButton(
                  onPressed: () => Navigator.pop(context, 'Cancel'),
                  child: const Text('Cancel'),
                ),
                TextButton(
                  onPressed: () => Navigator.pop(context, 'Yes'),
                  child: const Text('Yes'),
                ),
              ],
            ),
          ),
          child: const Text('Log In'),
        ),
        ElevatedButton(
          child: const Text('Upload Files'),
          onPressed: () {
            ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(
                content:
                    const Text('Your files have been uploaded successfully.'),
                action: SnackBarAction(
                  label: 'Dismiss',
                  onPressed: () {
                    // Code to execute.
                  },
                ),
              ),
            );
          },
        ),
        ElevatedButton(
          onPressed: showToast,
          style: ElevatedButton.styleFrom(
            primary: Colors.grey[800],
          ),
          child: Text('Push Code'),
        )
      ],
    );
  }

  void showToast() {
    Fluttertoast.showToast(
        msg: "Warning : The code you have written is badly formatted",
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.BOTTOM_RIGHT,
        timeInSecForIosWeb: 4,
        backgroundColor: Colors.red,
        textColor: Colors.white,
        fontSize: 16.0);
  }
}
